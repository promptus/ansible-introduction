# Ansible introduction

install ansible

    https://docs.ansible.com/ansible/latest/installation_guide/intro_installation.html

install vagrant

    https://www.vagrantup.com/docs/installation/

Start the vagrant boxes

    vagrant up web1
    vagrant up web2
    vagrant up db

play around with ansible, for example

    ansible-playbook -i development combined.yml

will run all playbooks on all vagrant boxes